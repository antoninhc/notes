package com.frerejacques.notesapp.recyclerViewAdaptater;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.frerejacques.notesapp.Model.CardCategoryCrossRefModel;
import com.frerejacques.notesapp.Model.CategoryViewModel;
import com.frerejacques.notesapp.R;
import com.frerejacques.notesapp.data.Card;
import com.frerejacques.notesapp.data.CardCategoryCrossRef;
import com.frerejacques.notesapp.data.Category;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.List;

public class RecyclerViewAdapterCategoriesCard extends RecyclerView.Adapter<RecyclerViewAdapterCategoriesCard.ViewHolder> {

    private final List<Category> categoryList;
    private final List<Category> categoryListActive;
    private final Card card;
    private final Context c;
    public RecyclerViewAdapterCategoriesCard(Context c, List<Category> categoryList,List<Category> categoryListActive, Card card) {
        this.categoryList = categoryList;
        this.c = c;
        this.categoryListActive = categoryListActive;
        this.card = card;
    }

    @NonNull
    @Override
    public RecyclerViewAdapterCategoriesCard.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_category_add_to_card, parent, false);
        return new RecyclerViewAdapterCategoriesCard.ViewHolder(view);
    }


    @Override
    public void onBindViewHolder(@NonNull RecyclerViewAdapterCategoriesCard.ViewHolder holder, int position) {
        Category category = categoryList.get(position);

        holder.nameText.setText(category.name);
        holder.aSwitch.setChecked(categoryListActive.contains(category));

        holder.aSwitch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (holder.aSwitch.isChecked()){
                    CardCategoryCrossRefModel.insert(new CardCategoryCrossRef(category.category_id, card.card_id));
                }else{
                    CardCategoryCrossRefModel.delete(new CardCategoryCrossRef(category.category_id, card.card_id));
                }
            }
        });


    }

    @Override
    public int getItemCount() {
        return categoryList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        public Switch aSwitch;
        public TextView nameText;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            aSwitch = itemView.findViewById(R.id.card_switchline);
            nameText = itemView.findViewById(R.id.card_textView);

        }
    }
}
