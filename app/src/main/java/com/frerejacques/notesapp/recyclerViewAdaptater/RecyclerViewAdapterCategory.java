package com.frerejacques.notesapp.recyclerViewAdaptater;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.frerejacques.notesapp.R;
import com.frerejacques.notesapp.data.Category;
import com.google.android.material.chip.Chip;

import java.util.List;

public class RecyclerViewAdapterCategory extends RecyclerView.Adapter<RecyclerViewAdapterCategory.ViewHolder> {

    private final List<Category> categoryList;
    private final FragmentActivity c;
    public RecyclerViewAdapterCategory(FragmentActivity c, List<Category> categoryList) {
        this.categoryList = categoryList;
        this.c = c;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.categorie_filter, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerViewAdapterCategory.ViewHolder holder, int position) {
        Category category = categoryList.get(position);

        holder.dateChip.setText(category.name);
    }

    @Override
    public int getItemCount() {
        return categoryList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        public Chip dateChip;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            dateChip = itemView.findViewById(R.id.chip4);

        }
    }
}