package com.frerejacques.notesapp.recyclerViewAdaptater;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import com.frerejacques.notesapp.Model.CardViewModel;
import com.frerejacques.notesapp.Model.CategoryViewModel;
import com.frerejacques.notesapp.R;
import com.frerejacques.notesapp.SecondFragment;
import com.frerejacques.notesapp.data.Category;
import com.google.android.material.chip.Chip;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.text.DateFormat;
import java.util.List;
import java.util.Locale;

public class RecyclerViewAdapterCategoryEdit  extends RecyclerView.Adapter<RecyclerViewAdapterCategoryEdit.ViewHolder> {

    private final List<Category> categoryList;
    private final Context c;
    public RecyclerViewAdapterCategoryEdit(Context c, List<Category> categoryList) {
        this.categoryList = categoryList;
        this.c = c;
    }

    @NonNull
    @Override
    public RecyclerViewAdapterCategoryEdit.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_category_edit_line, parent, false);
        return new RecyclerViewAdapterCategoryEdit.ViewHolder(view);
    }


    @Override
    public void onBindViewHolder(@NonNull RecyclerViewAdapterCategoryEdit.ViewHolder holder, int position) {
        Category category = categoryList.get(position);

        holder.nameText.setText(category.name);
        holder.aSwitch.setChecked(category.is_active);




        holder.buttonSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                category.name = holder.nameText.getText().toString();
                CategoryViewModel.update(category);
            }
        });

        holder.aSwitch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                category.is_active = holder.aSwitch.isChecked();
                CategoryViewModel.update(category);
            }
        });

        holder.buttonDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CategoryViewModel.delete(category);
            }
        });


    }

    @Override
    public int getItemCount() {
        return categoryList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        public Switch aSwitch;
        public EditText nameText;
        public FloatingActionButton buttonSave;
        public FloatingActionButton buttonDelete;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            aSwitch = itemView.findViewById(R.id.switchline);
            nameText = itemView.findViewById(R.id.textline);
            buttonSave = itemView.findViewById(R.id.saveline);
            buttonDelete = itemView.findViewById(R.id.deleteline);

        }
    }
}
