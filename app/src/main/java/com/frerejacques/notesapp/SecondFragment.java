package com.frerejacques.notesapp;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LiveData;
import androidx.navigation.Navigation;
import androidx.navigation.fragment.NavHostFragment;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.frerejacques.notesapp.Model.CardViewModel;
import com.frerejacques.notesapp.data.Card;
import com.frerejacques.notesapp.data.CategoryDao;
import com.frerejacques.notesapp.databinding.FragmentSecondBinding;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

public class SecondFragment extends Fragment {

    private FragmentSecondBinding binding;
    public  static Card selectedCard =null;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {

        binding = FragmentSecondBinding.inflate(inflater, container, false);
        View root = binding.getRoot();


        final FloatingActionButton fab =  getActivity().findViewById(R.id.fab);
        if (fab !=null) fab.hide();

        return root;

    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        if (selectedCard !=null){
          binding.description.setText(selectedCard.description);
          binding.title2.setText(selectedCard.title);

            binding.buttonOk.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    selectedCard.description = binding.description.getText().toString();
                    selectedCard.title = binding.title2.getText().toString();
                    if (selectedCard.title.matches("")) {
                        Toast.makeText(getActivity(), "Please enter a valid title", Toast.LENGTH_SHORT).show();

                    }else{

                        CardViewModel.update(selectedCard);

                        NavHostFragment.findNavController(SecondFragment.this)
                                .navigate(R.id.action_SecondFragment_to_FirstFragment);
                    }

                }
            });
            binding.cardButtonCat.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    CategoiesCardActivity.card = selectedCard;
                    Intent intent = new Intent(getContext(), CategoiesCardActivity.class);
                    startActivity(intent);
                }
            });
            binding.shareButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    RequestQueue queue = Volley.newRequestQueue(getContext());
                    String url = "https://pastebin.com/api/api_post.php";
                    StringRequest postRequest = new StringRequest(Request.Method.POST, url,
                            new Response.Listener<String>()
                            {
                                @Override
                                public void onResponse(String response) {
                                    // response
                                    String []res = response.split("/");
                                    new AlertDialog.Builder(getContext())
                                            .setTitle("Code à partager :")
                                            .setMessage(res[res.length-1])
                                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int which) {
                                                }
                                            })
                                            .setNegativeButton(android.R.string.no, null)
                                            .setIcon(android.R.drawable.ic_menu_share)
                                            .show();
                                }
                            },
                            new Response.ErrorListener()
                            {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    // error
                                    Log.d("Error.Response","");
                                }
                            }
                    ) {
                        @Override
                        protected Map<String, String> getParams()
                        {
                            Map<String, String>  params = new HashMap<String, String>();
                            params.put("api_dev_key", "m6JDF8hYZs2oGsgdgVsgiIFkGv8SCaPP");
                            params.put("api_paste_expire_date", "10M");
                            params.put("api_paste_code", selectedCard.title+"@@@@@@@@@@"+selectedCard.description);
                            params.put("api_option", "paste");
                            return params;
                        }
                    };
                    queue.add(postRequest);



                }
            });

        }
        else{
            binding.cardButtonCat.hide();
            binding.shareButton.hide();
            binding.buttonOk.setText("Add");
            binding.buttonOk.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String des = binding.description.getText().toString();
                    String title = binding.title2.getText().toString();
                    if (title.matches("")) {
                        Toast.makeText(getActivity(), "Please enter a valid title", Toast.LENGTH_SHORT).show();

                    }else{

                        CardViewModel.insert(new Card(title, "red", des, Calendar.getInstance().getTime()));

                        NavHostFragment.findNavController(SecondFragment.this)
                                .navigate(R.id.action_SecondFragment_to_FirstFragment);
                    }

                }
            });

        }



    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        selectedCard = null;
        binding = null;
    }

}