package com.frerejacques.notesapp;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.frerejacques.notesapp.Model.CardViewModel;
import com.frerejacques.notesapp.Model.CategoryViewModel;
import com.frerejacques.notesapp.data.Card;
import com.frerejacques.notesapp.data.Category;
import com.frerejacques.notesapp.databinding.FragmentFirstBinding;
import com.frerejacques.notesapp.recyclerViewAdaptater.RecyclerViewAdapter;
import com.frerejacques.notesapp.recyclerViewAdaptater.RecyclerViewAdapterCategory;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;

public class FirstFragment extends Fragment {

    private FragmentFirstBinding binding;

    private CardViewModel cardViewModel;
    private CategoryViewModel categoryViewModel;


    private RecyclerView recyclerView;
    private RecyclerView recyclerView1;
    private RecyclerViewAdapter recyclerViewAdapter;
    private RecyclerViewAdapterCategory recyclerViewAdapter1;


    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {



        binding = FragmentFirstBinding.inflate(inflater, container, false);
        View root = binding.getRoot();


        final FloatingActionButton fab =  getActivity().findViewById(R.id.fab);
        if (fab !=null) fab.show();

        //Recycle view Cat
        categoryViewModel = new ViewModelProvider.AndroidViewModelFactory(
                getActivity().getApplication()).create(CategoryViewModel.class);

        recyclerView1 = root.findViewById(R.id.list_cat);
        recyclerView1.setHasFixedSize(true);
        recyclerView1.setLayoutManager(new LinearLayoutManager(getActivity().getApplicationContext(), LinearLayoutManager.HORIZONTAL, false));

        categoryViewModel.getAllCategoriesActive().observe(getViewLifecycleOwner(),cats -> {
            recyclerViewAdapter1 = new RecyclerViewAdapterCategory(getActivity(), cats);
            recyclerView1.setAdapter(recyclerViewAdapter1);
        });


        //recycle view Card

        cardViewModel = new ViewModelProvider.AndroidViewModelFactory(
                getActivity().getApplication()).create(CardViewModel.class);


        recyclerView = root.findViewById(R.id.list_of_card_overview);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity().getApplicationContext()));


        cardViewModel.getAllCard().observe(getViewLifecycleOwner(),cards -> {




            categoryViewModel.getAllCategoriesActive().observe(getViewLifecycleOwner(),cats -> {
                if(cats.size()> 0){
                    List<Card> l = new ArrayList<>();
                    for (Category c : cats){
                        List<Card> temp = categoryViewModel.getCard(c);
                        for (Card c_temp : temp){
                            if(!l.contains(c_temp)){
                                l.add(c_temp);
                            }
                        }

                    }
                    recyclerViewAdapter = new RecyclerViewAdapter(getActivity(), l);
                }else{
                    recyclerViewAdapter = new RecyclerViewAdapter(getActivity(), cards);

                }

                recyclerView.setAdapter(recyclerViewAdapter);
            });
        });


        binding.button11.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(), CategoryActivity.class);
                startActivity(intent);
            }
        });


        return root;

    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

}