package com.frerejacques.notesapp.Model;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.frerejacques.notesapp.AppDatabase;
import com.frerejacques.notesapp.data.Card;
import com.frerejacques.notesapp.data.CardDao;
import com.frerejacques.notesapp.data.Category;
import com.frerejacques.notesapp.data.CategoryDao;

import java.util.List;

public class CategoryViewModel extends AndroidViewModel {
    private static CategoryDao categoryDao;
    private static LiveData<List<Category>> allCategories;

    public CategoryViewModel(@NonNull Application application){
        super(application);
        AppDatabase database = AppDatabase.getDatabase(application);
        categoryDao = database.categoryDao();

        allCategories = categoryDao.getAll();
    }

    public static void insert(Category category){
        AppDatabase.databaseWriterExecutor.execute(()->  categoryDao.insert(category));

    }
    public  LiveData<List<Category>> getAllCategories(){
        return allCategories;
    }

    public  LiveData<List<Category>> getAllCategoriesActive(){
        return categoryDao.getAllActive();
    }

    public static void delete(Category category){
        AppDatabase.databaseWriterExecutor.execute(()-> categoryDao.delete(category));
    }
    public static void update(Category category){
        AppDatabase.databaseWriterExecutor.execute(()-> categoryDao.update(category));
    }
    public List<Card> getCard(Category category){
        return categoryDao.getCardsById(category.category_id).cards;
    }
}
