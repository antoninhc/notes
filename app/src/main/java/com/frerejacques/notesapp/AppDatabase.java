package com.frerejacques.notesapp;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;
import androidx.sqlite.db.SupportSQLiteDatabase;

import com.frerejacques.notesapp.data.Card;
import com.frerejacques.notesapp.data.CardCategoryCrossRef;
import com.frerejacques.notesapp.data.CardCategoryCrossRefDao;
import com.frerejacques.notesapp.data.CardDao;
import com.frerejacques.notesapp.data.Category;
import com.frerejacques.notesapp.data.CategoryDao;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Database(entities = {Card.class, Category.class, CardCategoryCrossRef.class}, version = 4, exportSchema = false)
@TypeConverters({Converter.class})
public abstract class AppDatabase extends RoomDatabase {
    public static final int NUMBER_OF_TRHEAD = 4;
    public static final String DATABASE_NANE = "Note_DATABASE";

    public static volatile AppDatabase INSTANCE;

    public static final ExecutorService databaseWriterExecutor =
            Executors.newFixedThreadPool(NUMBER_OF_TRHEAD);

    public static final  RoomDatabase.Callback sRoomDataBaseCallback =
            new RoomDatabase.Callback(){
                @Override
                public void onCreate(@NonNull SupportSQLiteDatabase db){
                    super.onCreate(db);
                    databaseWriterExecutor.execute(()->{
                        CardDao cardDao = INSTANCE.cardDao();
                        cardDao.deleteAll();

                        CategoryDao categoryDao = INSTANCE.categoryDao();
                        categoryDao.deleteAll();


                        CardCategoryCrossRefDao cardCategoryCrossRefDao = INSTANCE.cardCategoryCrossRefDao();
                        cardCategoryCrossRefDao.deleteAll();
                    });
                }
            };

    public  static AppDatabase getDatabase(final Context context){
        if (INSTANCE == null){
            synchronized (AppDatabase.class){
                INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                        AppDatabase.class, DATABASE_NANE).addCallback(sRoomDataBaseCallback).fallbackToDestructiveMigration().allowMainThreadQueries().build();
            }
        }
        return INSTANCE;
    }


   public abstract CardDao cardDao();
   public abstract CategoryDao categoryDao();
   public abstract CardCategoryCrossRefDao cardCategoryCrossRefDao();
}
